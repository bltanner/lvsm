# LVSM - README
## Version
### Current Version: alpha 0.1
* Darkens Block Diagrams and subdiagrams
* Darkens comments and labels

## Setup
### On Windows
* Clone this repository to \Documents\LabVIEW Data\Quick Drop Plugins
       
``` 
	cd Documents\LabVIEW Data\Quick Drop Plugins

	git clone https://bltanner@bitbucket.org/bltanner/lvsm.git
```
	
* In LabVIEW, select a hotkey for the quickdrop plugin (default is Ctrl-$)

## Dependencies
LVSM only depends on vi.lib (LabVIEW builtins).
This plugin has only been tested on LabVIEW 2018, your mileage may vary on other versions.

## Contribution Guidelines
To contribute, simply create a branch to implement a bug fix or new feature.
All Merges and pull requests will be reviewed and approved in the order received.

## Support
For support, please email **bryson.tanner@sisu.us** for support.

Bug reports should have a subject line starting with "LVSM Bug" 

Feature requests should have a sibject line starting with "LVSM New Feature"